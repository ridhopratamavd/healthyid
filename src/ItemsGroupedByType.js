import React from 'react';
import {FlatList, SafeAreaView, Text, View} from 'react-native';
import Item from './Item';

function ItemsGroupedByType({itemType, items}) {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "space-around",
        alignItems: "flex-start",
        padding: "5%",
        borderWidth: 4,
        borderColor: "rgba(250,247,247,0.76)",
      }}>

      <Text style={{
        flex: 1,
        alignItems: "flex-start",
        fontSize: 15,
        fontWeight: "bold"
      }}>{itemType}</Text>

      <SafeAreaView style={{flex: 1, width:330}}>
        <FlatList
          data={items}
          renderItem={({item}) => <Item title={item.itemName}/>}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>
  )
}

export default ItemsGroupedByType;
