import React from 'react';
import {Text, View} from 'react-native';

function Item({title}) {
  function capitalizeFLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <View>
      <Text style={{fontSize: 11}}>{capitalizeFLetter(title)}</Text>
    </View>
  );
}

export default Item;
