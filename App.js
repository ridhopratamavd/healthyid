/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ItemsGroupedByType from "./src/ItemsGroupedByType";

const App: () => React$Node = () => {
  return (
    <View style={{flex: 1, paddingTop: 40}}>
      <View style={{backgroundColor: "#84d6af", height: 60, padding: 15}}>
        <Text style={{flex: 1, alignItems: "center", paddingTop: 5}}>HealthyId</Text>
      </View>
      <View style={{flex: 1}}>
        <ItemsGroupedByType itemType={'Topping'} items={[
          {itemName: 'raisin', id: 1},
          {itemName: 'granolla', id: 2}]}/>
        <ItemsGroupedByType itemType={'Yoghurt'}/>
        <ItemsGroupedByType itemType={'Fruits'}/>
      </View>
    </View>

  );
};

const styles = StyleSheet.create({});

export default App;
